import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable, switchMap } from 'rxjs';
import { Patient } from 'src/app/model/patient';
import { VitalSign } from 'src/app/model/vitalSign';
import { PatientService } from 'src/app/service/patient.service';
import { VitalSignService } from 'src/app/service/vital-sign.service';

@Component({
  selector: 'app-vital-sign-edit',
  templateUrl: './vital-sign-edit.component.html',
  styleUrls: ['./vital-sign-edit.component.css']
})
export class VitalSignEditComponent {

  form: FormGroup;
  id: number;
  isEdit: boolean;
  minDate: Date = new Date();
  dateSelected: Date;
  patients$: Observable<Patient[]>;
  idPatientSelected: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private vitalSignService: VitalSignService,
    private patientService: PatientService
  ){

  }

  ngOnInit(): void {
    this.form = new FormGroup({
      idVitalSign: new FormControl(0),
      patient: new FormControl('', [Validators.required, Validators.minLength(1)]),
      temperature: new FormControl('', [Validators.required, Validators.minLength(1)]),
      pulse: new FormControl('', [Validators.required, Validators.minLength(1)]),
      heartbeat: new FormControl(''),
      date: new FormControl(moment(this.dateSelected).format('YYYY-MM-DDTHH:mm:ss'))
    });

    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.isEdit = data['id'] != null;
      this.initForm();
    });
    
    this.getInitialData();
  }

  getInitialData() {
    this.patients$ = this.patientService.findAll(); //.subscribe(data => this.patients$ = data);
  }
  
  initForm(){
    if(this.isEdit){
      this.vitalSignService.findById(this.id).subscribe(data => {
        this.form = new FormGroup({
          idVitalSign: new FormControl(data.idVitalSign),
          patient: new FormControl(data.patient.idPatient, [Validators.required, Validators.minLength(3)]),
          temperature: new FormControl(data.temperature, [Validators.required, Validators.minLength(1)]),
          pulse: new FormControl(data.pulse, [Validators.required, Validators.minLength(1)]),
          heartbeat: new FormControl(data.heartbeat),
          date: new FormControl(moment(this.dateSelected).format('YYYY-MM-DDTHH:mm:ss')),
        });
      });
    }
  }
  
  get f() {
    return this.form.controls;
  }

  
  operate() {
    if (this.form.invalid) { return; }

    const patient: Patient = {
      idPatient: this.form.value['patient']
    };

    let vitalSign = new VitalSign();
    vitalSign.idVitalSign = this.form.value['idVitalSign'];
    vitalSign.patient = patient
    vitalSign.temperature = this.form.value['temperature'];
    vitalSign.pulse = this.form.value['pulse'];
    vitalSign.heartbeat = this.form.value['heartbeat'];
    vitalSign.date = this.form.value['date'];
    

    if (this.isEdit) {
      //UPDATE
      //PRACTICA COMUN
      this.vitalSignService.update(vitalSign.idVitalSign, vitalSign).subscribe(() => {
        this.vitalSignService.findAll().subscribe(data => {
          this.vitalSignService.setVitalSignChange(data);
          this.vitalSignService.setMessageChange('UPDATED!')
        });
      });
    } else {      
      //INSERT
      //PRACTICA IDEAL
      this.vitalSignService.save(vitalSign).pipe(switchMap(()=>{        
        return this.vitalSignService.findAll();
      }))
      .subscribe(data => {
        this.vitalSignService.setVitalSignChange(data);
        this.vitalSignService.setMessageChange("CREATED!")
      });
    }
    this.router.navigate(['/pages/vital-sign']);
  }

  cancel() {
    this.router.navigate(['/pages/vital-sign']).then(() => {
      // Coloca aquí el código presente en el método ngOnInit()
      // que deseas ejecutar al redirigir a la ruta
    });
  }
  
  retrieveDate(e: any) {
    console.log(e);
  }
}
