import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { switchMap } from 'rxjs';
import { FilterConsultDTO } from 'src/app/dto/filterConsultDTO';
import { VitalSign } from 'src/app/model/vitalSign';
import { VitalSignService } from 'src/app/service/vital-sign.service';

@Component({
  selector: 'app-vital-sign',
  templateUrl: './vital-sign.component.html',
  styleUrls: ['./vital-sign.component.css']
})
export class VitalSignComponent  implements OnInit {

  form: FormGroup;
  dataSource: MatTableDataSource<VitalSign>
  displayedColumns = ['patient', 'temperature', 'pulse', 'heartbeat', 'date', 'actions'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @ViewChild('tab') tabGroup: MatTabGroup;

  constructor(
    private route: ActivatedRoute,
    private vitalSignService: VitalSignService
  ) {}

  ngOnInit(): void {
    this.vitalSignService.getVitalSignChange().subscribe(data => {
      this.createTable(data);
    });
    this.findAll();
    this.form = new FormGroup({
      dni: new FormControl(),
      fullname: new FormControl(),
      startDate: new FormControl(),
      endDate: new FormControl(),
    });
  }

  findAll() {
    this.vitalSignService.findAll().subscribe(data => {
      this.createTable(data);
    });
  }

  search() {
      const dni = this.form.value['dni'];
      const fullname = this.form.value['fullname'];
      const dto = new FilterConsultDTO(dni, fullname);

      if(dto.dni == null){
        delete dto.dni;
      }

      if(dto.fullname == null){
        delete dto.fullname;
      }

      console.log(dto);
      this.vitalSignService.searchOthers(dto).subscribe(data => {
        this.createTable(data);
      });
  }
  

  createTable(data: VitalSign[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  

  delete(idPatient: number){
    this.vitalSignService.delete(idPatient).pipe(switchMap( ()=> {
      return this.vitalSignService.findAll();
    })).subscribe(data => {
      this.vitalSignService.setVitalSignChange(data);
      this.vitalSignService.setMessageChange('DELETED!');
    });
  }
  

  checkChildren(): boolean{
    return this.route.children.length != 0;
  }
}
