import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService } from 'src/app/service/login.service';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit  {

  username: string;
  role: string;

  constructor(
    // private menuService: MenuService,
    // private router: Router,
    private loginService: LoginService 
  ) {}

  ngOnInit(): void {
    // this.menuService.getMenuChange().subscribe((data) => (this.menus = data));

    const helper = new JwtHelperService();
    const decodeToken = helper.decodeToken(sessionStorage.getItem(environment.TOKEN_NAME));
    console.log(decodeToken);
    
    this.username = decodeToken.sub; //preferred_username
    this.role = decodeToken.role; //preferred_username
  }
}
