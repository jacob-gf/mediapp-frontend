import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; //>4.3 HttpClient
import { environment } from 'src/environments/environment.development';
import { Subject } from 'rxjs';
import { GenericService } from './generic.service';
import { VitalSign } from '../model/vitalSign';
import { FilterConsultDTO } from '../dto/filterConsultDTO';


@Injectable({
  providedIn: 'root'
})
export class VitalSignService extends GenericService<VitalSign>{

  //private url: string = `${environment.HOST}/patients`;

  //VARIABLE REACTIVA
  private vitalSignChange = new Subject<VitalSign[]>;
  private messageChange = new Subject<string>;

  constructor(protected override http: HttpClient){
    super(http, `${environment.HOST}/vital-sign`)
  }

  listPageable(p: number, s: number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  //constructor(private http: HttpClient) { }

  /*findAll() {
    return this.http.get<Patient[]>(this.url);
  }

  findById(id: number) {
    return this.http.get<Patient>(`${this.url}/${id}`);
  }

  save(patient: Patient){
    return this.http.post(this.url, patient);
  }

  update(id: number, patient: Patient){
    return this.http.put(`${this.url}/${id}`, patient);
  }

  delete(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }*/

  //////////////// get set //////////////////
  getVitalSignChange(){
    return this.vitalSignChange.asObservable();
  }

  setVitalSignChange(data: VitalSign[]){
    this.vitalSignChange.next(data);
  }

  getMessageChange(){
    return this.messageChange.asObservable();
  }

  setMessageChange(data: string){
    this.messageChange.next(data);
  }


  searchOthers(dto: FilterConsultDTO){
    return this.http.post<VitalSign[]>(`${this.url}/search/others`, dto);
  }
  searchByDates(date1: string, date2: string){
    /*let params: HttpParams = new HttpParams();
    params.set('date1', date1);
    params.set('date2', date2);

    return this.http.get<Consult[]>(`${this.url}/search/date`, {
      params: params
    });*/

    return this.http.get<VitalSign[]>(`${this.url}/search/date?date1=${date1}&date2=${date2}`)
  }
}
